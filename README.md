# README 

  Modified librsync-2.0.0 for (Arch Linux) mingw-w64 cross toolchain

## NOTES

Changes in the latest version:

	- librsync-2.0.0 CMakeLists.txt modified to correct DLL installation
      path.
    - sha356sum in PKGBUILD modified accordingly.
    - Modified librsync-2.0.0 added to repo.

## TODO: 

- Modified librsync-2.0.0 must be removed later 
- Changes to librsync-2.0.0 CMakeLists.txt must be replaced with a patch.

